Rails.application.routes.draw do
  root to: 'search#index'

  match 'search', to: "search#search", via: [:get, :post], as: :search

  resources :routes
  resources :nodes

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
