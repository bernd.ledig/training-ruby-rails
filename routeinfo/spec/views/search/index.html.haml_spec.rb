require 'rails_helper'

RSpec.describe "search/index.html.haml", type: :view do
  before(:each) do
    # Zuweisen der vom View genutzten Attribute
    assign :search, OpenStruct.new(:source => 'XXX', :destination => 'YYY')
    render # Rendered den View, Ergebnis ist per "rendered" abrufbar
  end

  it "should have the header" do
    expect(rendered).to match(/<h2>Suche Verbindung<\/h2>/)
  end

end
