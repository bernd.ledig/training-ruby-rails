require 'rails_helper'

RSpec.describe SearchController, type: :controller do

  describe "GET #index" do
    before do
      get :index
    end

    it "should be successful" do
      expect(response).to have_http_status(:success)
    end

    it "should have a search-object" do
      # assigns enthält die Controller-Attribute als Hash
      expect(assigns[:search]).to_not be_nil
    end
  end

end
