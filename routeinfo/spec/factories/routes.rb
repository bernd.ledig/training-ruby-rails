FactoryBot.define do
  factory :route do
    source { "MyString" }
    destination { "MyString" }
    distance { 1 }
  end
end
