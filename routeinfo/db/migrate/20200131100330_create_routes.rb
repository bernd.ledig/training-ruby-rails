class CreateRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.string :source, null: false
      t.string :destination, null: false
      t.integer :distance, null: false

      t.timestamps
      t.index [:source, :destination], unique: true
    end
  end
end
