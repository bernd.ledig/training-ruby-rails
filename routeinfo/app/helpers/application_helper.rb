module ApplicationHelper

  # Formatieren einer Zahl (Tausender-Trenner) und anhängen der Einheit "km"
  # Parameter:
  #   distance:  Distance als ganze Zahl (darf nil sein)
  # Returns:
  #   String mit der formatierten Distance (bei nil => leerer String)
  def format_km distance
    return "" if distance.nil?
    number_with_delimiter(distance, :separator => ".")+" km"
  end


end
