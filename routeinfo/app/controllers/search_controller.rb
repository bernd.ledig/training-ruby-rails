class SearchController < ApplicationController
  def index                   # nutzt per Konvention den View search/index.html...
    @search = OpenStruct.new  # die Weitergabe von Daten an das View erfolgt
                              # über Instance-Attribute
  end

  def search
    @search = OpenStruct.new params[:search]
    logger.debug "Start search with: #{@search.to_json}"
    if (@search.source =~ /^a/i)
      @route = OpenStruct.new(:source => @search.source,
                              :vertexes => ['Hannover', 'Berlin'],
                              :destination => @search.destination,
                              :distance => 560)
    else
      @route = :not_found
    end
    render :action => :index
  end
end
